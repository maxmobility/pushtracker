# PushTracker

This device is designed to detect pushes on a manual wheelchair by
using an accelerometer.  It provides user interaction and feedback
through a rotary+push switch and a 128x128 pixel color OLED.